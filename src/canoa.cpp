#include "canoa.hpp"
#include "embarcacoes.hpp"
#include <iostream>
#include <bits/stdc++.h>

Canoa::Canoa(){
	set_tipo("canoa");
	set_tamanho(1);
}

Canoa::~Canoa(){
	
}

long Canoa::get_linha(){
	return linha;
}

long Canoa::get_coluna(){
	return coluna;
}

void Canoa::set_local(long posicaox, long posicaoy){   //set_local ira definir a posição da canoa no mapa
	this->linha = posicaox;
	this->coluna = posicaoy;
}