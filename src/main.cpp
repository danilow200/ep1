#include <iostream>
#include <string>
#include <fstream>
#include "jogador.hpp"
#include "embarcacoes.hpp"
#include "mapa.hpp"
#include "canoa.hpp"
#include "submarino.hpp"
#include "porta_avioes.hpp"

using namespace std;

int main(){
	Jogador jogador1; //Primeiro jogador
	Jogador jogador2; //Segundo jogador
	Mapa mapa1; //Mapa visível para o primeiro jogador
	Mapa mapa2; //Mapa visível para o segundo jogadoro
	string arquivo;  //O nome do arquivo que sera lido para definir as posições das embarcações
	ifstream(leitordotxt);

	string name; //Variavel auxiliar para receber os nomes dos jogadores

	system("clear"); //Comando utilizado no terminal do linux para limpar a tela

	cout << "O jogo a seguir é uma batalha naval criada como projeto de Orientação a Objetos da UNB - FGA" << endl;
	system("sleep 3");  //Comando utilizado no terminal do linux que faz o terminal espera a quantidade de segundos determinado, nesse caso foi 3 segundos
	system("clear");
	cout << "Bem vindo jogadores" << endl;
	system("sleep 3");
	system("clear");

	cout << "Insira o nome do primeiro jogador" << endl;
	cin >> name;
	jogador1.set_nome(name);
	cout << "Bem vindo ";
	jogador1.imprimir_nome();
	system("sleep 3");
	system("clear");

	cout << "Insira o nome do segundo jogador" << endl;
	cin >> name;
	jogador2.set_nome(name);
	cout << "Bem vindo ";
	jogador2.imprimir_nome();
	system("sleep 3");
	system("clear");

	//Parte do codigo dedica a leitura do arquivo de localização das embarcações
	cout << "Insira o nome do aruivo de mapa desejado" << endl;
	cin >> arquivo;
	
	string linha;
	leitordotxt.open(arquivo);
	
	long posicaox[24], posicaoy[24];
	char direcao[24];
	int i = 0;
	
	if(leitordotxt.is_open()){
		while(getline(leitordotxt, linha)){
			
			if( linha[1] == ' '  && linha[0] >= 48 && linha[0] < 66) {
				posicaox[i] = linha[0] - 48 ;
				
				if(linha[3] == ' ') {
					posicaoy[i] = linha[2] - 48;
					if (i < 6 || (i > 11 && i < 18) ) direcao[i] = linha[10];
					else if (i < 10 || (i > 17 && i < 22)) direcao[i] = linha[14];
					else direcao[i] = linha[17];
				}
				else {
					if (i < 6 || (i > 11 && i < 18) )direcao[i] = linha[11];
					else if (i < 10 || (i > 17 && i < 21)) direcao[i] = linha[15];
					else direcao[i] = linha[18];
					posicaoy[i] =(linha[2] - 48)* 10 + linha[3] - 48;
				}
				i++;
			}
			else if (linha[1] != ' '  && linha[0] >= 48 && linha[0] < 66){
				posicaox[i] = (linha[0] - 48)*10 + linha[1] - 48;

				if(linha[4] == ' ') {
					if (i < 6 || (i > 11 && i < 18) )direcao[i] = linha[11];
					else if (i < 10 || (i > 17 && i < 21)) direcao[i] = linha[15];
					else direcao[i] = linha[18];
					posicaoy[i] = linha[3] - 48;
				}
				else{
					if (i < 6 || (i > 11 && i < 18) )direcao[i] = linha[12];
					else if (i < 10 || (i > 17 && i < 22)) direcao[i] = linha[16];
					else direcao[i] = linha[19];
					posicaoy[i] = (linha[3] - 48)* 10 + linha[4] - 48;
				}
				i++;
			}
			
		}

	}
	
	//Arrarys de 0 a 5 pertence ao mapa 1, e de 6 a 11 pertence ao mapa 2
	Canoa canoa[12];
	canoa[0].set_local(posicaox[0], posicaoy[0]);
	canoa[1].set_local(posicaox[1], posicaoy[1]);
	canoa[2].set_local(posicaox[2], posicaoy[2]);
	canoa[3].set_local(posicaox[3], posicaoy[3]);
	canoa[4].set_local(posicaox[4], posicaoy[4]);
	canoa[5].set_local(posicaox[5], posicaoy[5]);
	canoa[6].set_local(posicaox[12], posicaoy[12]);
	canoa[7].set_local(posicaox[13], posicaoy[13]);
	canoa[8].set_local(posicaox[14], posicaoy[14]);
	canoa[9].set_local(posicaox[15], posicaoy[15]);
	canoa[10].set_local(posicaox[16], posicaoy[16]);
	canoa[11].set_local(posicaox[17], posicaoy[17]);

	//Arrarys de 0 a 3 pertence ao mapa 1, e de 6 a 9 pertence ao mapa 2
	Submarino submarino[12];
	submarino[0].set_local(posicaox[6],posicaoy[6],direcao[7]);
	submarino[1].set_local(posicaox[7],posicaoy[7],direcao[7]);
	submarino[2].set_local(posicaox[8],posicaoy[8],direcao[8]);
	submarino[3].set_local(posicaox[9],posicaoy[9],direcao[9]);
	submarino[6].set_local(posicaox[18],posicaoy[18],direcao[18]);
	submarino[7].set_local(posicaox[19],posicaoy[19],direcao[19]);
	submarino[8].set_local(posicaox[20],posicaoy[20],direcao[20]);
	submarino[9].set_local(posicaox[21],posicaoy[21],direcao[21]);

	//Arrarys 0 e 1 pertence ao mapa 1, e 6 e 7 pertence ao mapa 2
	Porta_avioes porta_avioes[12];
	porta_avioes[0].set_local(posicaox[10], posicaoy[10], direcao[10]);
	porta_avioes[1].set_local(posicaox[11], posicaoy[11], direcao[11]);
	porta_avioes[6].set_local(posicaox[22], posicaoy[22], direcao[22]);
	porta_avioes[7].set_local(posicaox[23], posicaoy[23], direcao[23]);

	//Fim da parte da leitura de arquivo

	//Introdução da interface para o jogador
	system("clear");
	cout << "A seguir vem o confronto" << endl;
	cout << "O tabuleiro é formado por numeros e letras, a onde os numeros são as linhas e as letras as colunas" << endl;
	cout << "Casas com 'X' são locais que o jogador acertou porem não possuia nada" << endl;
	cout << "Casas com 'C' são locais a onde foi afundada uma canoa" << endl;
	cout << "Casas com 'S' são locais a onde foi acertada uma parte do submarino" << endl;
	cout << "Casas com 'P' são locais a onde foi acertada uma parte do porta aviões" << endl;
	cout << "Tenha um otimo confronto" << endl;
	system("sleep 10");

	for(;;){
		int numero;
		char letra;
		Embarcacoes embarcacao;
		int jogadas1 = 1, jogadas2 = 1; //Quando o jogador será zerado o valor e jogador perdera a vez

		//Laço de repetição dedica a jogada do jogador 1
		for(;;){
			system("sleep 3");
			system("clear");

			mapa2.imprime_mapa();
			cout << endl << jogador1.get_nome() << "             Score:" << jogador1.get_score() << endl;
			cout << endl << jogador1.get_nome() << " escolha um numero e uma letra em maisculo que estejam diponivel no tabuleiro" << endl;
			cin >> numero >> letra;
			embarcacao.set_eixo_y(letra);
			embarcacao.set_eixo_x(numero);

			if(jogador1.get_score() == 21)
				break;
			
			for(int i=6;i<12;i++){
				if(embarcacao.get_eixo_x() == canoa[i].get_linha() && embarcacao.converco_letra(letra) == canoa[i].get_coluna()){
					mapa2.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'C');
					cout << endl << "Canoa acertada" << endl;
					jogador1.score_soma(1);
					break;
				}
				else if(i <= 9){
					for(int j=0;j<4;j++){
						if(j<2){
							if(embarcacao.get_eixo_x() == submarino[i].get_linha(j) && embarcacao.converco_letra(letra) == submarino[i].get_coluna(j)){
								cout << "Submarino acertado" << endl;
								mapa2.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'S');
								jogador1.score_soma(1);
								break;
							}
						}
						if(i <= 7){
							if(embarcacao.get_eixo_x() == porta_avioes[i].get_linha(j) && embarcacao.converco_letra(letra) == porta_avioes[i].get_coluna(j)){
								cout << "Porta aviões acertado" << endl;
								mapa2.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'P');
								jogador1.score_soma(1);
								break;
							}
						}
					}
				}
				else if(mapa2.get_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y()) == '_'){
					mapa2.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'X');
					jogadas1 = 0;
					cout << "Errou" << endl;
					break;
				}
			}
			if(jogadas1 == 0)
				break;
		}
		if(jogador1.get_score() == 21){
			cout << jogador1.get_nome() << " Ganhou!" << endl;
			break;
		}

		//Laço de repetição dedica a jogada do jogador 2
		for(;;){
			system("sleep 3");
			system("clear");

			mapa1.imprime_mapa();
			cout << endl << jogador2.get_nome() << "             Score:" << jogador2.get_score() << endl;
			cout << endl << jogador2.get_nome() << " escolha um numero e uma letra em maisculo que estejam diponivel no tabuleiro" << endl;
			cin >> numero >> letra;
			embarcacao.set_eixo_y(letra);
			embarcacao.set_eixo_x(numero);

			if(jogador2.get_score() == 21)
				break;
			
			for(int i=0;i<6;i++){
				if(embarcacao.get_eixo_x() == canoa[i].get_linha() && embarcacao.converco_letra(letra) == canoa[i].get_coluna()){
					mapa1.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'C');
					cout << endl << "Canoa acertada" << endl;
					jogador2.score_soma(1);
					break;
				}
				else if(i <= 3){
					for(int j=0;j<4;j++){
						if(j<2){
							if(embarcacao.get_eixo_x() == submarino[i].get_linha(j) && embarcacao.converco_letra(letra) == submarino[i].get_coluna(j)){
								cout << "Submarino acertado" << endl;
								mapa1.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'S');
								jogador2.score_soma(1);
								break;
							}
						}
						if(i <= 1){
							if(embarcacao.get_eixo_x() == porta_avioes[i].get_linha(j) && embarcacao.converco_letra(letra) == porta_avioes[i].get_coluna(j)){
								cout << "Porta aviões acertado" << endl;
								mapa1.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'P');
								jogador2.score_soma(1);
								break;
							}
						}
					}
				}
				else if(mapa1.get_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y()) == '_'){
					mapa1.set_tabuleiro(embarcacao.get_eixo_x()+1, embarcacao.get_eixo_y(), 'X');
					jogadas2 = 0;
					cout << "Errou" << endl;
					break;
				}
			}
			if(jogadas2 == 0)
				break;
		}
		if(jogador2.get_score() == 21){
			cout << jogador2.get_nome() << " Ganhou!" << endl;
			break;
		}

	}
	system("sleep 3");
	system("clear");

	cout << "Obrigado por jogar" << endl;

	system("sleep 3");
	system("clear");

    return 0;
}
