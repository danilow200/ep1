#include "jogador.hpp"
#include <iostream>

Jogador::Jogador(){
	nome = "";
	score = 0;
}

Jogador::~Jogador(){

}

string Jogador::get_nome(){
	return nome;
}
void Jogador::set_nome(string nome){
	this->nome = nome;
}

long Jogador::get_score(){
	return score;
}
void Jogador::set_score(long score){
	this->score = score;
}

void Jogador::imprimir_nome(){
	cout << nome << endl;
}

void Jogador::score_soma(int i){
	score += i;
}