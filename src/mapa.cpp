#include "mapa.hpp"
#include <iostream>
#include <string.h>

Mapa::Mapa(){
	//Inicio do gerador de mapa
	letra = "    A B C D E F G H I J K L M";
	for(int i=0;i<13;i++)
		numero[i] = i;
	for(int i=0;i<=26;i++){
			if(i==1 || i==3 || i==5 || i==7 || i==9 || i==11 || i==13 || i==15 || i==17 || i==19 || i==21 || i==23 || i==25)
				tabuleiro[0][i] = '_';
			else
				tabuleiro[0][i] = ' ';
	}
	for(int i=1;i<=13;i++){
			tabuleiro[i][0] = '|';
		for(int j=1;j<=26;j++){
			if(j==1 || j==3 || j==5 || j==7 || j==9 || j==11 || j==13 || j==15 || j==17 || j==19 || j==21 || j==23 || j==25)
				tabuleiro[i][j] = '_';
			else
				tabuleiro[i][j] = '|';
		}
	}
	//Fim do gerador de mapa
}
Mapa::~Mapa(){

}

char Mapa::get_tabuleiro(long i, long j){
	return tabuleiro[i][j];
}
void Mapa::set_tabuleiro(long i, long j, char letra){
	this->tabuleiro[i][j] = letra;
}

void Mapa::imprime_mapa(){
	//Função que irar imprimir o mapa visivel para os jogadores;
	cout << letra << endl;
	for(int i=0;i<=13;i++){
		if (i<=10 && i>0)
			cout << numero[i-1] << "  ";
		else if (i>10 && i>0)
			cout << numero[i-1] << " ";
		else
			cout << "   ";
		for(int j=0;j<=26;j++)
			cout << tabuleiro[i][j];
		cout << endl;
	}
		
}