#include "embarcacoes.hpp"
#include <iostream>

Embarcacoes::Embarcacoes(){
	tipo = "";
	eixo_x = 0;
	eixo_y = 0;
	tamanho = 0;
}
Embarcacoes::~Embarcacoes(){
	
}

string Embarcacoes::get_tipo(){
	return tipo;
}
void Embarcacoes::set_tipo(string tipo){
	this->tipo = tipo;
}

long Embarcacoes::get_eixo_x(){
	return eixo_x;
}
void Embarcacoes::set_eixo_x(long eixo_x){
	this->eixo_x = eixo_x;
}

long Embarcacoes::get_eixo_y(){
	return eixo_y;
}
void Embarcacoes::set_eixo_y(char letra){    //Ira receber uma letra e ira passar para cordenadas validas dentro da matriz criada para o mapa
	this->eixo_y = (letra-65)+1+(letra-65);
}

long Embarcacoes::get_tamanho(){
	return tamanho;
}
void Embarcacoes::set_tamanho(long tamanho){
	this->tamanho = tamanho;
}

char Embarcacoes::converco_letra(char letra){
	return letra-65;
}
