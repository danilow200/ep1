#include "porta_avioes.hpp"
#include "embarcacoes.hpp"
#include <iostream>

Porta_avioes::Porta_avioes(){
	set_tipo("porta avioes");
	set_tamanho(4);
}

Porta_avioes::~Porta_avioes(){
	
}

long Porta_avioes::get_linha(int i){
	return linha[i];
}

long Porta_avioes::get_coluna(int i){
	return coluna[i];
}

void Porta_avioes::set_local(long posicaox, long posicaoy, char direcao){  //set_local ira definir a posição do porta aviões no mapa
	if (direcao == 'd'){ 				//Se a direção for para a direita
		for (int i = 0; i < 4; i++){
			this->linha[i] = posicaox;
			this->coluna[i] = posicaoy + i;
		}

	}
	else if(direcao == 'e'){			//Se a direção for para a esquerda
		for (int i = 0; i < 4; i++){
			this->linha[i] = posicaox;
			this->coluna[i] = posicaoy - i;
		}
	}
	else if(direcao == 'c'){			//Se a direção for para cima
		for (int i = 0; i < 4; i++){
			this->linha[i] = posicaox - i;
			this->coluna[i] = posicaoy;
		}
	}
	else if(direcao == 'b'){			//Se a direção for para baixo
		for (int i=0;i < 4; i++){
			this->linha[i] = posicaox + i;
			this->coluna[i] = posicaoy;
		}
	}
}