#include "submarino.hpp"
#include "embarcacoes.hpp"
#include <iostream>
#include <bits/stdc++.h>

Submarino::Submarino(){
	set_tipo("submarino");
	set_tamanho(2);
}

Submarino::~Submarino(){
	
}

long Submarino::get_linha(int i){
	return linha[i];
}

long Submarino::get_coluna(int i){
	return coluna[i];
}

void Submarino::set_local(long posicaox, long posicaoy, char direcao){  //set_local ira definir a posição do submarino no mapa
	if (direcao == 'd'){ 				//Se a direção for para a direita
		for (int i = 0; i < 2; i++){
			this->linha[i] = posicaox;
			this->coluna[i] = posicaoy + i;
		}

	}
	else if(direcao == 'e'){			//Se a direção for para a esquerda
		for (int i = 0; i < 2; i++){
			this->linha[i] = posicaox;
			this->coluna[i] = posicaoy - i;
		}
	}
	else if(direcao == 'c'){			//Se a direção for para cima
		for (int i = 0; i < 2; i++){
			this->linha[i] = posicaox - i;
			this->coluna[i] = posicaoy;
		}
	}
	else if(direcao == 'b'){			//Se a direção for para baixo
		for (int i=0;i < 2; i++){
			this->linha[i] = posicaox + i;
			this->coluna[i] = posicaoy;
		}
	}
}