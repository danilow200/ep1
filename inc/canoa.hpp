#ifndef CANOA_HPP
#define CANOA_HPP

#include "embarcacoes.hpp"
#include <string>
#include <iostream>
 
 using namespace std;

class Canoa : public Embarcacoes{
 	private:
 		long linha;
 		long coluna;

 	public:
 	Canoa();
 	~Canoa();

 	long get_linha();
 	long get_coluna();

 	void set_local(long linha, long coluna);
 };


#endif