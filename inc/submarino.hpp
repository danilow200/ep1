#ifndef SUBMARINO_HPP
#define SUBMARINO_HPP

#include "embarcacoes.hpp"
#include <string>
#include <iostream>
 
 using namespace std;

class Submarino : public Embarcacoes{
 	private:
 		long linha[2];
 		long coluna[2];

 	public:
 	Submarino();
 	~Submarino();

 	long get_linha(int i);
 	long get_coluna(int i);

 	void set_local(long linha, long coluna, char direcao);
 };


#endif