#ifndef MAPA_HPP
#define MAPA_HPP

#include <iostream>
#include <string.h>

using namespace std;

class Mapa{
	private:
		string letra; //letra de localização do tabuleiro
		long numero[13]; //numero de localização do tabuleiro
		char tabuleiro[50][50];
	public:
		Mapa();
		~Mapa();

		char get_tabuleiro(long i, long j);
		void set_tabuleiro(long i, long j, char letra);

		void imprime_mapa();
	
		
};


#endif
