#ifndef PORTA_AVIOES_HPP
#define PORTA_AVIOES_HPP

#include "embarcacoes.hpp"
#include <string>
#include <iostream>
 
 using namespace std;

class Porta_avioes : public Embarcacoes{
 	private:
 		
 	public:
 		long linha[4];
 		long coluna[4];
 		
 		Porta_avioes();
 		~Porta_avioes();

 		long get_linha(int i);
 		long get_coluna(int i);

 		void set_local(long linha, long coluna, char direcao);
 };


#endif