#ifndef EMBARCACOES_HPP
#define EMBARCACOES_HPP

#include <string>
#include <iostream>

using namespace std;

class Embarcacoes{
	private:
		string tipo;
		long eixo_x; //Localização da embarcação no tabuleiro do jogo no eixo x
		long eixo_y; //Localização da embarcação no tabuleiro do jogo no eixo y
		long tamanho;
	public:
		Embarcacoes();
		~Embarcacoes();

		string get_tipo();
		void set_tipo(string tipo);

		long get_eixo_x();
		void set_eixo_x(long eixo_x);

		long get_eixo_y();
		void set_eixo_y(char eixo_letra);

		long get_tamanho();
		void set_tamanho(long tamanho);

		char converco_letra(char letra); //Converço de letra do mapa para o numero no eixo y

};

#endif
