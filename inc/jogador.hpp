#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include <string>

using namespace std;

class Jogador {
	private:
		string nome;
		long score; //Pontuação do jogador
	public:
		Jogador();
		~Jogador();

		string get_nome();
		void set_nome(string nome);

		long get_score();
		void set_score(long score);

		void score_soma(int i); //Responsavel por adicionar pontos ao jogador

		void imprimir_nome();  //Ira imprimir o nome do jogador em certas ocasiões

};

#endif
